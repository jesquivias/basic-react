import React, {Component} from 'react';
import Header from './Header';
import Products from './Products';
import Footer from './Footer';

class App extends Component {
	state = {
			products : []
	};

	componentDidMount() {
		// variable with products

		const products = [
			{name: 'Book', price: 200},
			{name: 'CD of music', price: 100},
			{name: 'Guitar', price: 800},
			{name: 'MP3 player', price: 1500},
			{name: 'Album', price: 500}
		]

		setTimeout( () => {
			this.setState({
				products : products
			});
		}, 3000);
	}
	render() {

		return (
			<div>
				<Header
					title="Our Virtual Store"
				/>
				<Products
					products={this.state.products}
				/>
				<Footer />
			</div>
		)
	}
}

export default App;
